Kumar's ConTeXt Slides
======================

Instructions: To use this, edit `slides.tex` and run the following
commands:

	make get-fonts # to get the fonts; one run should be good enough
	make
