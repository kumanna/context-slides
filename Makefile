all: slides.pdf

slides.pdf: slides.tex env-slides.tex
	context slides

.PHONY: clean get-fonts
clean:
	$(RM) slides.pdf

base_dir := $(shell pwd)

get-fonts:
	mkdir -p fonts
	cd fonts && \
	wget -c "http://www.exljbris.com/dl/fontin2_pc.zip" && \
	unzip -o fontin2_pc.zip && \
	wget -c "http://www.exljbris.com/dl/delicious-123.zip" && \
	unzip -o delicious-123.zip && \
	wget -c https://github.com/khaledhosny/euler-otf/raw/master/euler.otf && \
	OSFONTDIR=$(base_dir)/fonts/ mtxrun --script fonts --reload && \
	cd ..
